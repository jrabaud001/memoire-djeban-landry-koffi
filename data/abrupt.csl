<?xml version="1.0" encoding="utf-8"?>
<style xmlns="http://purl.org/net/xbiblio/csl" class="note" default-locale="fr-FR" version="1.0" page-range-format="expanded">
  <info>
    <title>Style bibliographique Abrüpt</title>
    <title-short>Abrüpt</title-short>
    <id>https://abrupt.cc</id>
    <link href="https://abrupt.cc" rel="self"/>
    <link href="https://gitlab.com/cestabrupt/gabarit-abrupt" rel="documentation"/>
    <author>
      <name>Abrüpt</name>
      <email>ecrire@abrupt.cc</email>
    </author>
    <category citation-format="note"/>
    <category field="social_science"/>
    <category field="generic-base"/>
    <updated>2021-08-25</updated>
    <rights license="https://creativecommons.org/licenses/by-sa/4.0/">This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 License</rights>
  </info>
  <locale xml:lang="fr">
    <terms>
      <term name="ordinal">e</term>
      <term name="ordinal-01" gender-form="feminine" match="whole-number">re</term>
      <term name="ordinal-01" gender-form="masculine" match="whole-number">er</term>
      <term name="cited">op. cit.</term>
      <term name="ibid">ibid.</term>
      <term name="page" form="short">p.</term>
      <term name="folio" form="short">
        <single>fᵒ</single>
        <multiple>fᵒˢ</multiple>
      </term>
      <term name="issue" form="short">
        <single>nᵒ</single>
        <multiple>nᵒˢ</multiple>
      </term>
      <term name="editor" form="short">
        <single>éd.</single>
        <multiple>éds</multiple>
      </term>
      <term name="no date">s.d.</term>
      <term name="et-al">et al.</term>
      <term name="in">dans</term>
    </terms>
  </locale>
  <macro name="author">
    <names variable="author" font-variant="normal">
      <et-al font-style="italic"/>
      <name form="long" and="text" delimiter-precedes-last="never" sort-separator=" ">
        <name-part name="family" font-variant="small-caps"/>
      </name>
      <label form="short" prefix=" (" suffix=")"/>
      <substitute>
        <names variable="editor"/>
        <names variable="translator"/>
        <text macro="title"/>
      </substitute>
    </names>
  </macro>
  <macro name="author-bib">
    <names variable="author">
      <name name-as-sort-order="all" form="long" and="text" delimiter-precedes-last="never" sort-separator=" ">
        <name-part name="family" font-variant="small-caps"/>
      </name>
      <label form="short" prefix=" (" suffix=")"/>
      <substitute>
        <names variable="editor"/>
        <names variable="translator"/>
        <text macro="title"/>
      </substitute>
    </names>
  </macro>
  <macro name="author-ibid">
    <names variable="author">
      <name form="long" and="text" delimiter-precedes-last="never" sort-separator=" ">
        <name-part name="family" font-variant="small-caps"/>
      </name>
      <label form="short" prefix=" (" suffix=")"/>
      <substitute>
        <names variable="editor"/>
        <names variable="translator"/>
        <text macro="title"/>
      </substitute>
    </names>
  </macro>
  <macro name="container-author">
    <names variable="container-author">
      <name form="long" and="text" delimiter-precedes-last="never" sort-separator=" ">
        <name-part name="family" font-variant="small-caps"/>
      </name>
    </names>
  </macro>
  <macro name="editor">
      <choose>
        <if variable="container-author">
          <names variable="editor">
            <label form="short" prefix=" " suffix=" "/>
            <name form="long" and="text" delimiter-precedes-last="never" sort-separator=" ">
              <name-part name="family" font-variant="small-caps"/>
            </name>
          </names>
        </if>
        <else>
          <names variable="editor">
            <name form="long" and="text" delimiter-precedes-last="never" sort-separator=" ">
              <name-part name="family" font-variant="small-caps"/>
            </name>
            <label form="short" prefix=" (" suffix=")"/>
          </names>
        </else>
      </choose>
  </macro>
  <macro name="translator">
    <names variable="translator" prefix=" traduit par ">
      <name form="long" and="text" delimiter-precedes-last="never" sort-separator=" ">
        <name-part name="family" font-variant="small-caps"/>
      </name>
    </names>
  </macro>
  <macro name="title">
    <choose>
      <if type="bill book graphic legal_case motion_picture report song article" match="any">
        <group delimiter=" ">
          <text variable="title" text-case="capitalize-first" font-style="italic"/>
          <choose>
            <if variable="status" match="any">
              <text variable="status" prefix="[" suffix="]"/>
            </if>
          </choose>
        </group>
      </if>
      <else-if type="article-journal article-newspaper article-magazine" match="any">
        <group delimiter=", ">
          <group delimiter=" ">
            <text variable="title" text-case="capitalize-first" quotes="true"/>
            <choose>
              <if variable="status" match="any">
                <text variable="status" prefix="[" suffix="]"/>
              </if>
            </choose>
          </group>
          <text variable="container-title" font-style="italic"/>
        </group>
      </else-if>
      <else-if type="thesis" match="any">
        <group delimiter="">
          <group delimiter=" ">
            <text variable="title" text-case="capitalize-first" font-style="italic" suffix=","/>
            <choose>
              <if variable="status" match="any">
                <text variable="status" prefix="[" suffix="]"/>
              </if>
            </choose>
          </group>
          <text variable="genre" suffix=", " prefix=" "/>
          <text variable="publisher"/>
        </group>
      </else-if>
      <else-if type="manuscript" match="any">
        <group delimiter=",">
          <group delimiter=" ">
            <text variable="title" text-case="capitalize-first" font-style="italic"/>
            <choose>
              <if variable="status" match="any">
                <text variable="status" prefix="[" suffix="]"/>
              </if>
            </choose>
          </group>
          <text variable="genre" prefix=" [" suffix="]"/>
        </group>
      </else-if>
      <else-if type="chapter entry-dictionary entry-encyclopedia" match="any">
        <choose>
          <if variable="container-author">
            <group delimiter="">
              <text variable="title" text-case="capitalize-first" font-style="italic"/>
              <text term="in" suffix=" " prefix=", "/>
              <choose>
                <if variable="genre" match="none">
                  <text macro="container-author" suffix=", "/>
                </if>
              </choose>
              <group delimiter=" ">
                <text variable="container-title" text-case="capitalize-first" font-style="italic"/>
                <choose>
                  <if variable="status" match="any">
                    <text variable="status" prefix="[" suffix="]"/>
                  </if>
                </choose>
              </group>
              <text macro="editor" prefix=", " suffix=", "/>
            </group>
          </if>
          <else>
            <group delimiter="">
              <text variable="title" text-case="capitalize-first" quotes="true" suffix=","/>
              <text term="in" suffix=" " prefix=" "/>
              <text macro="editor" suffix=", "/>
              <group delimiter=" ">
                <text variable="container-title" text-case="capitalize-first" font-style="italic"/>
                <choose>
                  <if variable="status" match="any">
                    <text variable="status" prefix="[" suffix="]"/>
                  </if>
                </choose>
              </group>
            </group>
          </else>
        </choose>
      </else-if>
      <else-if type="webpage post-weblog article" match="any">
        <group delimiter="">
          <group delimiter=" ">
            <text variable="title" text-case="capitalize-first" font-style="italic" suffix=", "/>
            <choose>
              <if variable="status" match="any">
                <text variable="status" prefix="[" suffix="]"/>
              </if>
            </choose>
          </group>
          <text variable="URL"/>
        </group>
      </else-if>
      <else>
        <group delimiter=" ">
          <text variable="title" quotes="true"/>
          <choose>
            <if variable="status" match="any">
              <text variable="status" prefix="[" suffix="]"/>
            </if>
          </choose>
        </group>
      </else>
    </choose>
    <choose>
      <if variable="subtitle" match="any">
        <text value=" : " font-style="italic"/>
        <text variable="subtitle" font-style="italic"/>
      </if>
    </choose>
  </macro>
  <macro name="note">
    <choose>
      <if variable="note" match="any">
        <text variable="note"/>
      </if>
    </choose>
  </macro>
  <macro name="addendum">
    <choose>
      <if variable="addendum" match="any">
        <text variable="addendum"/>
      </if>
    </choose>
  </macro>
  <macro name="publisher">
    <choose>
      <if type="bill book chapter entry-dictionary entry-encyclopedia graphic legal_case motion_picture paper-conference report song webpage" match="any">
        <text variable="publisher"/>
      </if>
    </choose>
  </macro>
  <macro name="pub-place">
    <choose>
      <if type="bill book chapter entry-dictionary entry-encyclopedia thesis graphic legal_case manuscript motion_picture paper-conference report song" match="any">
        <choose>
          <if variable="publisher-place" match="any">
            <text variable="publisher-place"/>
          </if>
          <else>
            <text value="s.l."/>
          </else>
        </choose>
      </if>
    </choose>
  </macro>
  <macro name="yearpage">
    <choose>
      <if type="bill book graphic legal_case motion_picture paper-conference manuscript report song thesis" match="any">
        <group delimiter=", ">
          <group delimiter=" ">
            <date variable="issued" form="numeric" date-parts="year"/>
            <date variable="original-date" form="numeric" date-parts="year" prefix="[" suffix="]"/>
          </group>
          <group>
            <text term="volume" form="short" suffix=". "/>
            <text variable="volume"/>
          </group>
          <choose>
            <if variable="locator" match="any">
              <group delimiter=" ">
                <label variable="locator" form="short"/>
                <text variable="locator"/>
              </group>
            </if>
          </choose>
        </group>
      </if>
      <else-if type="chapter entry-dictionary entry-encyclopedia" match="any">
        <group delimiter=" ">
          <group delimiter=" " suffix=",">
            <date variable="issued" form="numeric" date-parts="year"/>
            <date variable="original-date" form="numeric" date-parts="year" prefix="[" suffix="]"/>
          </group>
          <group>
            <text term="volume" form="short" suffix=". "/>
            <text variable="volume" suffix=","/>
          </group>
          <group delimiter=", ">
            <choose>
              <if variable="locator" match="any">
                <group delimiter=", ">
                  <group delimiter=" ">
                    <label variable="page" form="short"/>
                    <text variable="page"/>
                  </group>
                  <group delimiter=" ">
                    <label variable="locator" form="short"/>
                    <text variable="locator"/>
                  </group>
                </group>
              </if>
              <else-if variable="locator" match="none">
                <group delimiter=" ">
                  <label variable="page" form="short"/>
                  <text variable="page"/>
                </group>
              </else-if>
            </choose>
          </group>
        </group>
      </else-if>
      <else-if type="article-journal" match="any">
        <group delimiter=", ">
          <choose>
            <if variable="locator" match="any">
              <group delimiter=", ">
                <group delimiter=" ">
                  <label variable="page" form="short"/>
                  <text variable="page"/>
                </group>
                <group delimiter=" ">
                  <label variable="locator" form="short"/>
                  <text variable="locator"/>
                </group>
              </group>
            </if>
            <else-if variable="locator" match="none">
              <group delimiter=" ">
                <label variable="page" form="short"/>
                <text variable="page"/>
              </group>
            </else-if>
          </choose>
        </group>
      </else-if>
      <else-if type="article-newspaper article-magazine" match="any">
        <date variable="issued">
          <date-part name="day" suffix=" "/>
          <date-part name="month" form="short" suffix=" "/>
          <date-part name="year"/>
        </date>
        <group font-style="normal" delimiter=" " prefix=", ">
          <label variable="page" form="short"/>
          <text variable="page"/>
        </group>
        <group delimiter=" " font-style="normal">
          <choose>
            <if variable="locator" match="any">
              <group delimiter=" ">
                <label variable="locator" form="short"/>
                <text variable="locator"/>
              </group>
            </if>
          </choose>
        </group>
      </else-if>
      <else-if type="webpage post-weblog" match="any" delimiter=", ">
        <group suffix=", ">
          <date variable="issued">
            <date-part name="day" form="ordinal" suffix=" "/>
            <date-part name="month" suffix=" "/>
            <date-part name="year"/>
          </date>
        </group>
        <group>
          <text value="consulté le" suffix=" "/>
          <date variable="accessed">
            <date-part name="day" form="ordinal" suffix=" "/>
            <date-part name="month" suffix=" "/>
            <date-part name="year"/>
          </date>
        </group>
      </else-if>
    </choose>
  </macro>
  <macro name="yearpage-bib">
    <choose>
      <if type="bill book graphic legal_case motion_picture paper-conference report song thesis" match="any">
        <group delimiter=", ">
          <group delimiter=", ">
            <group delimiter=" ">
              <date variable="issued" form="numeric" date-parts="year"/>
              <date variable="original-date" form="numeric" date-parts="year" prefix="[" suffix="]"/>
            </group>
          </group>
          <group delimiter=" ">
            <label variable="locator" form="short"/>
            <text variable="locator"/>
          </group>
        </group>
      </if>
      <else-if type="chapter entry-dictionary entry-encyclopedia" match="any">
        <group delimiter=", ">
          <group delimiter=" ">
            <date variable="issued" form="numeric" date-parts="year"/>
            <date variable="original-date" form="numeric" date-parts="year" prefix="[" suffix="]"/>
          </group>
          <choose>
            <if variable="locator" match="any">
              <group delimiter=", ">
                <group delimiter=" ">
                  <label variable="page" form="short"/>
                  <text variable="page"/>
                </group>
                <group delimiter=" ">
                  <label variable="locator" form="short"/>
                  <text variable="locator"/>
                </group>
              </group>
            </if>
            <else-if variable="locator" match="none">
              <group delimiter=" ">
                <label variable="page" form="short"/>
                <text variable="page"/>
              </group>
            </else-if>
          </choose>
        </group>
      </else-if>
      <else-if type="article-journal" match="any">
        <group delimiter=", ">
          <group delimiter=" ">
            <date variable="issued">
              <date-part name="day" form="ordinal" text-decoration="none" suffix=" "/>
              <date-part name="month" suffix=" "/>
              <date-part name="year"/>
            </date>
            <date variable="original-date" form="numeric" date-parts="year" prefix="[" suffix="]"/>
          </group>
          <choose>
            <if variable="locator" match="any">
              <group delimiter=", ">
                <group delimiter=" ">
                  <label variable="page" form="short"/>
                  <text variable="page"/>
                </group>
                <group delimiter=" ">
                  <label variable="locator" form="short"/>
                  <text variable="locator"/>
                </group>
              </group>
            </if>
            <else-if variable="locator" match="none">
              <group delimiter=" ">
                <label variable="page" form="short"/>
                <text variable="page"/>
              </group>
            </else-if>
          </choose>
        </group>
      </else-if>
      <else-if type="article-newspaper article-magazine" match="any">
        <group delimiter=" ">
          <date variable="issued" suffix=",">
            <date-part name="day" form="ordinal" suffix=" "/>
            <date-part name="month" suffix=" "/>
            <date-part name="year"/>
          </date>
          <label variable="page" form="short"/>
          <text variable="page"/>
        </group>
      </else-if>
      <else-if type="manuscript">
        <group delimiter="" font-style="normal">
          <choose>
            <if variable="issued">
              <date variable="issued">
                <date-part name="day" form="ordinal" suffix=" "/>
                <date-part name="month" suffix=" "/>
                <date-part name="year"/>
              </date>
            </if>
            <else>
              <text value="s.d."/>
            </else>
          </choose>
        </group>
      </else-if>
      <else-if type="webpage post-weblog" match="any" delimiter=", ">
        <group suffix=", ">
          <date variable="issued">
            <date-part name="day" form="ordinal" suffix=" "/>
            <date-part name="month" suffix=" "/>
            <date-part name="year"/>
          </date>
        </group>
        <group>
          <text value="consulté le" suffix=" "/>
          <date variable="accessed">
            <date-part name="day" form="ordinal" suffix=" "/>
            <date-part name="month" suffix=" "/>
            <date-part name="year"/>
          </date>
        </group>
      </else-if>
    </choose>
  </macro>
  <macro name="edition">
    <choose>
      <if type="bill book graphic legal_case motion_picture report song chapter paper-conference" match="any">
        <choose>
          <if is-numeric="edition">
            <group delimiter=" ">
              <number variable="edition" form="ordinal"/>
              <text term="edition" form="short"/>
            </group>
          </if>
          <else>
            <text variable="edition" text-case="capitalize-first" suffix="."/>
          </else>
        </choose>
      </if>
      <else-if type="article-journal article-magazine" match="any">
        <group delimiter="">
          <choose>
            <if variable="issued">
              <text macro="volume" suffix=", "/>
              <text macro="issue" suffix=", "/>
            </if>
            <else>
              <text macro="volume" text-case="capitalize-first" suffix=", "/>
              <text macro="issue" suffix=", "/>
            </else>
          </choose>
        </group>
      </else-if>
    </choose>
  </macro>
  <macro name="volume">
    <choose>
      <if is-numeric="volume">
        <text term="volume" form="short" suffix=". "/>
        <text variable="volume"/>
      </if>
      <else>
        <text variable="volume"/>
      </else>
    </choose>
  </macro>
  <macro name="issue">
    <choose>
      <if is-numeric="issue">
        <text term="issue" form="short" vertical-align="baseline" suffix=" "/>
        <text variable="issue"/>
      </if>
      <else>
        <text variable="issue"/>
      </else>
    </choose>
  </macro>
  <macro name="collection">
    <text variable="collection-title" quotes="true" prefix=" (coll. " suffix=")"/>
  </macro>
  <citation et-al-min="4" et-al-use-first="1">
    <layout suffix="." delimiter=" ; ">
      <choose>
        <if position="ibid-with-locator">
          <group delimiter=", ">
            <text term="ibid" text-case="capitalize-first" font-style="italic"/>
            <group delimiter=" ">
              <label variable="locator" form="short"/>
              <text variable="locator"/>
            </group>
          </group>
        </if>
        <else-if position="ibid">
          <text term="ibid" text-case="capitalize-first" font-style="italic"/>
        </else-if>
        <else-if position="subsequent">
          <group delimiter=", ">
            <text macro="author-ibid"/>
            <choose>
              <if type="bill chapter book graphic legal_case motion_picture report song thesis manuscript" match="any">
                <text variable="title" text-case="capitalize-first" form="short" font-style="italic"/>
                <text term="cited" font-style="italic" suffix="."/>
              </if>
              <else-if type="article-journal" match="any">
                <text variable="title" text-case="capitalize-first" form="short" quotes="true"/>
                <text value="art. cit."/>
              </else-if>
              <else>
                <text variable="title" text-case="capitalize-first" form="short" font-style="italic"/>
                <text term="cited" font-style="italic" suffix="."/>
              </else>
            </choose>
            <group delimiter=" ">
              <label variable="locator" form="short"/>
              <text variable="locator"/>
            </group>
          </group>
        </else-if>
        <else>
          <choose>
            <if type="manuscript">
              <group delimiter=", ">
                <text variable="archive"/>
                <text variable="archive_location"/>
                <text variable="call-number"/>
                <text macro="title"/>
                <text macro="note"/>
                <text macro="yearpage-bib"/>
                <text macro="addendum"/>
              </group>
            </if>
            <else-if type="bill book graphic legal_case motion_picture report song chapter paper-conference" match="any">
              <group delimiter=", ">
                <text macro="author"/>
                <text macro="title"/>
                <text macro="note"/>
                <text macro="translator"/>
                <text macro="edition"/>
                <text macro="volume"/>
                <group delimiter=" ">
                  <text macro="publisher"/>
                  <text macro="collection"/>
                </group>
                <text macro="pub-place"/>
                <text macro="yearpage-bib"/>
                <text macro="addendum"/>
              </group>
            </else-if>
            <else>
              <group delimiter=", ">
                <text macro="author"/>
                <text macro="title"/>
                <text macro="note"/>
                <text macro="translator"/>
                <text macro="edition"/>
                <text macro="publisher"/>
                <text macro="pub-place"/>
                <text macro="yearpage"/>
                <text macro="addendum"/>
              </group>
            </else>
          </choose>
        </else>
      </choose>
    </layout>
  </citation>
  <bibliography subsequent-author-substitute="—" subsequent-author-substitute-rule="complete-all" hanging-indent="true">
    <sort>
      <key macro="author" names-min="3" names-use-first="3"/>
      <key variable="issued" sort="descending"/>
    </sort>
    <layout suffix=".">
      <choose>
        <if type="manuscript">
          <group delimiter=", ">
            <text variable="archive"/>
            <text variable="archive_location"/>
            <text variable="call-number"/>
            <text macro="title"/>
            <text macro="note"/>
            <text macro="yearpage-bib"/>
            <text macro="addendum"/>
          </group>
        </if>
        <else-if type="bill book graphic legal_case motion_picture report song chapter paper-conference" match="any">
          <group delimiter=", ">
            <text macro="author-bib"/>
            <text macro="title"/>
            <text macro="note"/>
            <text macro="translator"/>
            <text macro="edition"/>
            <text macro="volume"/>
            <group delimiter=" ">
              <text macro="publisher"/>
              <text macro="collection"/>
            </group>
            <text macro="pub-place"/>
            <text macro="yearpage-bib"/>
            <text macro="addendum"/>
          </group>
        </else-if>
        <else>
          <group delimiter=", ">
            <text macro="author-bib"/>
            <text macro="title"/>
            <text macro="note"/>
            <text macro="translator"/>
            <text macro="edition"/>
            <group delimiter=" ">
              <text macro="publisher"/>
              <text macro="collection"/>
            </group>
            <text macro="pub-place"/>
            <text macro="yearpage-bib"/>
            <text macro="addendum"/>
          </group>
        </else>
      </choose>
    </layout>
  </bibliography>
</style>

