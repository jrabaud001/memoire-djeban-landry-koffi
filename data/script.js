$(window).on("scroll", function() {

    var currentPos = $(window).scrollTop();

    $('nav ul li a').each(function() {
        var sectionLink = $(this);
        var section = $(sectionLink.attr('href'));
        if(section.position().top <= currentPos && sectionLink.offset().top + section.height() >= currentPos) {
            $('nav ul li').removeClass('active');
            sectionLink.parent().addClass('active');
        }
        else {
            sectionLink.parent().removeClass('active');
        }
    });

});


function toggleTOC(id) {
    var n = document.getElementById(id);
    var n_ul = n.getElementsByTagName('ul')[0];
    if (n_ul.style.display == "none")
        {
            n_ul.style.display = "" ;
        } else {
            n_ul.style.display = "none" ;
        }
    };

function toggleNotes(b) {
    var r = document.querySelector(b);
    var r_ol = r.getElementsByTagName('ol')[0];
    if (r_ol.style.display == "none")
        {
            r_ol.style.display = "" ;
        } else {
            r_ol.style.display = "none" ;
        }
    };

function toggleBiblio(b) {
    var n = document.getElementById(b);
    if (n.style.display == "none")
        {
            n.style.display = "" ;
        } else {
            n.style.display = "none" ;
        }
}

function aff_masq(e) {
    var sect = document.getElementById(e);
    if (sect.innerHTML == "Afficher"){
        sect.innerHTML = "Masquer";
    } else {
        sect.innerHTML = "Afficher";
    }
};

function show(b) {
    var r = document.querySelector(b);
    var r_ol = r.getElementsByTagName('ol')[0];
    r_ol.style.display = "" ;
};

window.onload = function() {
    const elements = document.getElementsByClassName('footnote-ref');
    for (const element of elements) {
        element.addEventListener("click", e => {
            show('.footnotes');
            var sect = document.getElementById('aff-notes');
            sect.innerHTML = "Masquer";
        })
    };
};

$("#remerciements").nextUntil(":not(p)").addClass("ital");
