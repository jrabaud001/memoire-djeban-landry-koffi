Markdown - [Zettlr](https://www.zettlr.com) - Zotero - Pandoc - html - css - js - docx - pdf

---

[TOC]

---

## Fichiers sources

### Écriture avec [Zettlr](https://www.zettlr.com) dans les fichiers :

- `Remerciements.md`
- `Mémoire-1ere-Partie.md`
- `Mémoire-2eme-Partie-a.md`
- `Mémoire-2eme-Partie-b.md`
- `Mémoire-3eme-Partie.md`

### Bibliographie

#### Références bibliographiques avec Zotero

- Installation de l'extension [BetterBibTex for Zotero](https://retorque.re/zotero-better-bibtex/)
- Ajout des références du mémoire dans une collection `Mémoire` (par glisser-déposer d'autres collections ou ajout direct dans cette collection)
- Export de la collection au format `Better CSL JSON` 
  - cocher la case `Garder à jour`
  - vers le dossier `data`, fichier `Mémoire.json`
  - format préféré aux formats *tex* à cause du type de document `Entrée de dictionnaire`

#### Style des références et de la bibliographie (CSL)

- dans le dossier `data`, le fichier `transversalites-Braud.csl` : fichier modifié de `transversalites.csl` (dépôt officiel) pour :
    - distinguer *id. p. XX* et *ibid.* (même page)
    - distinguer les *op. cit.* et les *art. cit.*

#### Ajout comme dernières lignes du dernier fichier : 

```html
<div id="biblio">

### Bibliographie

::: {#refs}

:::

</div>
```

Pour pouvoir déplacer toute la biblio (y compris le titre) en CSS.

### Remerciements

Conversion du fichier `Remerciements.md` en fragments *html* et *latex* pour l'inclure avec l'option `--include-before-body`.

-  en html

```
pandoc -o remerciements.html Remerciements.md
```

- en latex

```
pandoc -o remerciements.tex Remerciements.md
```

## Conversion vers Word (.docx)

- Avec le fichier de type *default* `to_docx.yaml` bien renseigné (option *Pandoc* `-d file`).

<details>
    <summary style="color:royalblue">to_docx.yaml</summary>

```yaml
input-files:
- Mémoire-1ere-Partie.md
- Mémoire-2eme-Partie-a.md
- Mémoire-2eme-Partie-b.md
- Mémoire-3eme-Partie.md
metadata-file: meta.yaml
output-file: MémoireTotal.docx
standalone: true
toc: true
toc-depth: 5
number-sections: false
citeproc: true
metadata:
  bibliography: data/Mémoire.json
  csl: data/transversalites-Braud.csl
  notes-after-punctuation: false
variables:
  lang: "fr-FR"
reference-links: true
reference-doc: data/styleBraud.docx
```

</details>

- Exécuter dans le dossier (avec *WindowsPowerShell* ou *Bash*)

```sh
pandoc -d to_docx.yaml
```

Génère un fichier `MémoireTotal.docx`, que l'on a retravaillé "à la main" pour ajouter les remerciements et la page de garde avant de générer le pdf de soutenance (qui sera déposé dans Dumas)

#### fichiers de `./data` utilisés :

- `styleBraud.docx` : fichier word issu d'un export précoce où les styles ont été modifiés et qui sert de référence pour les styles Word.
- `Mémoire.json` : Le fichier qui contient les références bibliographiques (export synchronisé par *BetterBibTex* d'une collection *Zotero* éponyme).
- `transversalites-Braud.csl` : fichier de style pour les références bibliographiques et la bibliographie. Modifié de `transversalites.csl` (dépôt officiel) pour :
    - distinguer *id. p. XX* et *ibid.* (même page)
    - distinguer les *op. cit.* et les *art. cit.*


## Conversion en **html**

- Avec le fichier de type *default* `to_html.yaml` bien renseigné (option *Pandoc* `-d file`).

<details>
    <summary style="color:royalblue">to_html.yaml</summary>

```yaml
input-files:
- Mémoire-1ere-Partie.md
- Mémoire-2eme-Partie-a.md
- Mémoire-2eme-Partie-b.md
- Mémoire-3eme-Partie.md
metadata-file: meta.yaml
output-file: MémoireTotal2.html
template: data\template.html
include-before-body:
- remerciements.html
include-after-body: data\MémoireLandry_COinS.html
standalone: true
toc: true
toc-depth: 5
number-sections: false
citeproc: true
metadata:
  bibliography: data/Mémoire.json
  csl: data/transversalites-Braud.csl
  notes-after-punctuation: false
  link-citations: true
variables:
  lang: "fr-FR"
reference-links: true
```

</details>

- Exécuter dans le dossier (avec *WindowsPowerShell* ou *Bash*)

```sh
pandoc -d to_html.yaml
```

Génère un fichier `MémoireTotal2.html` qui pointe en relatif vers `data/style.css` et `data/script.js` et en ligne vers *hypothesis* et *jquery*.

#### fichiers de `./data` utilisés :

- `template.html` : 
    - utilise abondamment les valeurs de `meta.yaml` pour renseigner les éléments de la pseudo-couverture (mis en forme par `style.css`)
    - attention liens en dur vers le pdf et le dépôt gitlab
    - pour la css, un lien vers `data/style.css`
    - pour le js : 
        - intégration du lien vers le script *hypothesis* en ligne
        - intégration du lien vers la bibliothèque *jquery* en ligne
        - lien vers `data/script.js` (mise en évidence dans la table des matières de la section active)
- `Mémoire.json` : Le fichier qui contient les références bibliographiques (export synchronisé par *BetterBibTex* d'une collection *Zotero* éponyme).
- `transversalites-Braud.csl` : fichier de style pour les références bibliographiques et la bibliographie. Modifié de `transversalites.csl` (dépôt officiel) pour :
    - distinguer *id. p. XX* et *ibid.* (même page)
    - distinguer *op. cit.* et *art. cit.*
- `MémoireLandry_COinS.html` : export *Zotero* au format *COinS* de la collection des références bibliographiques du mémoire. Utilisé avec l'option `include-after-body` pour que les références soit détectables et récupérables par l'*extension Zotero du navigateur* lors de la consultation du mémoire en ligne. N'affiche rien de supplémentaire à l'écran : les références sont dans les propriétés de balises `<span>` vides. 

## Conversion en **pdf** (via latex)

- Avec le fichier de type *default* `to_pdf_tex.yaml` bien renseigné (option *Pandoc* `-d file`).

<details>
    <summary style="color:royalblue">to_pdf_tex.yaml</summary>

```yaml
input-files:
- Mémoire-1ere-Partie.md
- Mémoire-2eme-Partie-a.md
- Mémoire-2eme-Partie-b.md
- Mémoire-3eme-Partie.md
include-before-body:
- remerciements.tex
metadata-file: meta.yaml
shift-heading-level-by: -1
pdf-engine: xelatex
output-file: MémoireTotalv2.pdf
template: data\template.latex
standalone: true
toc: true
toc-depth: 5
top-level-division: part
number-sections: false
citeproc: true
metadata:
  bibliography: data/Mémoire.json
  csl: data/transversalites-Braud.csl
  notes-after-punctuation: false
  link-citations: true
variables:
  header-includes: |
    \usepackage[defaultlines=2,all]{nowidow}
    \usepackage{indentfirst}
  lang: "fr-FR"
  documentclass: scrbook
  classoption:
  - parskip: 0pt
  - 12pt
  - twoside
  - openright
  geometry:
  - top=30mm
  - bottom=25mm
  papersize: a4
  hyperrefoptions:
  - linktoc=all
  indent: true
  linestretch: 1.5
  pagestyle: headings
  # fontfamily: libertinus
  mainfont: "Times New Roman"
  sansfont: "Trebuchet MS"
  monofont: "Noto Mono"
  colorlinks: false
  logo: "data/logoAlter.png"
  book: true
  titlepage: true
  titlepage-rule-height: 0
  titlepage-background: "data/couv_background_tex.jpg"
  titlepage-logo: "data/logoAlter.png"
  logo-width: 40mm
reference-links: true
```

</details>

- Exécuter dans le dossier (avec *WindowsPowerShell* ou *Bash*)

```sh
pandoc -d to_pdf_tex.yaml
```

#### fichiers de `./data` utilisés :

- `template.latex` : utilise abondamment les valeurs de `meta.yaml` pour renseigner les éléments de la page de titre
- `couv_background_tex.jpg` : image de fond de la couverture (inclus le logo uppa)
- `Mémoire.json` : Le fichier qui contient les références bibliographiques (export synchronisé par *BetterBibTex* d'une collection *Zotero* éponyme).
- `transversalites-Braud.csl` : fichier de style pour les références bibliographiques et la bibliographie. Modifié de `transversalites.csl` (dépôt officiel) pour :
    - distinguer *id. p. XX* et *ibid.* (même page)
    - distinguer les *op. cit.* et les *art. cit.*

## Mettre en ligne (le dossier *Netlify*)

- Copier le résultat de la conversion *html* et le renommer `index.html`

- Copier le résultat de la conversion *pdf* dans `.\Netlify\data` (et le renommer comme dans `template.html`)

- Copier les fichiers images, `style.css` et `script.js` de `.\data` vers `.\Netlify\data` 

- Copier le dossier `Netlify` sur un serveur web. (https://master2-kristof.netlify.app/)


```mermaid
graph LR

  subgraph écriture
  id1(Markdown)
  end

  subgraph références-Zotero
  id7>.bib, .json, .yaml...]
  end

  subgraph Conversion
  id2((Pandoc))
  end
	
  subgraph édition
  id3(.docx)
  id3b(.odt)
  id4(.tex)
  end

  subgraph Publication
  id5(.html)
  id6(.pdf)
  id9(.epub)
  end

  id8{{+ .css, ...}}
   
  id1-->id2
  id7-->id2
  id2-->id3 & id3b & id4 & id8
  id8 -->id5 & id9
  id4-->id6

%%lien vers le manuel Pandoc
  click id2 "pandoc.org/MANUAL.html" _blank

%%lien interne vers une note dans obsidian
  class id1 internal-link

  style écriture fill:lightgreen
  style Conversion fill:lightpink
  style édition fill:lightblue
  style Publication fill:gold
  style références-Zotero fill:burlywood

  style id2 color:blue,stroke:deeppink,stroke-width:6px
  style id1 stroke:seagreen,stroke-width:6px
  style id7 stroke:crimson,stroke-width:3px
  style id8 stroke:darkturquoise,stroke-width:3px
	
  classDef ed stroke:cornflowerblue,stroke-width:3px
  class id3,id3b,id4 ed

  classDef pub stroke:darkorange,stroke-width:3px
  class id5,id6,id9 pub
```
